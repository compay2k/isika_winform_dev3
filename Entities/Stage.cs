﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.Isika.Dev3.Goelands.Entities
{
    public class Stage
    {
        public int Id { get; set; }
        public string Libelle { get; set; }
        public int Duree { get; set; }
        public int NbMinInscrits { get; set; }
        public int NbMaxInscrits { get; set; }
        public int IdNiveau { get; set; }
        public int? IdCategorieAge { get; set; }
    }
}
