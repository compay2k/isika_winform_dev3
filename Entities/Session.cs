﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.Isika.Dev3.Goelands.Entities
{
    public enum StatutSession
    {
        EnCoursDeCreation = 1,
        OuverteAuxInscriptions = 2,
        Cloturee = 3,
        Demarree = 4,
        Terminee = 5,
        Annulee = 6
    }

    public class Session
    {
        public int Id { get; set; }
        public DateTime DateDebut { get; set; }
        public float Tarif{ get; set; }
        public int IdStage { get; set; }
        public int? IdMoniteur { get; set; }
        public StatutSession Statut { get; set; }
    }
}
