﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.Isika.Dev3.Goelands.Entities
{
    public class Niveau
    {
        public int Id { get; set; }
        public string Libelle { get; set; }
        public string Description { get; set; }


        public string NomComplet
        {
            get
            {
                return Libelle + " - " + Description;
            }
        }
    }
}
