﻿using Fr.Isika.Dev3.Goelands.DataAccess;
using Fr.Isika.Dev3.Goelands.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.Isika.Dev3.Goelands.Test
{
    class Program
    {
        static void Main(string[] args)
        {
            NiveauDAO dao = new NiveauDAO();

            // insertion :
            Niveau newLevel = new Niveau();
            newLevel.Libelle = "niveau factorisation";
            newLevel.Description = "La factorisation c'est la vie !!";
            dao.Insert(newLevel);
            Console.WriteLine("nouvel id : " + newLevel.Id);

            // update : 
            //Niveau n = dao.GetById(5);
            //n.Libelle = "superstar";
            //n.Description = "dotnet c'est chouette";
            //dao.Update(n);

            
            List<Niveau> niveaux = dao.GetAll();

            foreach(Niveau niveau in niveaux)
            {
                Console.WriteLine(niveau.Libelle + " : "
                                + niveau.Description);
            }
            

            Console.ReadLine();
        }
    }
}
