﻿using Fr.Isika.Dev3.Goelands.DataAccess;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fr.Isika.Dev3.Goelands.Presentation
{
    public partial class ListeStages : Form
    {
        private StageDAO stageDao = new StageDAO();

        public ListeStages()
        {
            InitializeComponent();
        }

        private void ListeStages_Load(object sender, EventArgs e)
        {
            // bloquer l'édition :
            dgvStages.AllowUserToAddRows = false;
            dgvStages.AllowUserToDeleteRows = false;
            dgvStages.ReadOnly = true;
            // masquer l'entete de lignes :
            dgvStages.RowHeadersVisible = false;

            dgvStages.DataSource = stageDao.GetDetails();

            // colonne qui s'étire :
            dgvStages.Columns["libellé"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            // redimensionner colonnes :
            dgvStages.Columns["min."].Width = 50;
            dgvStages.Columns["max."].Width = 50;
            dgvStages.Columns["durée"].Width = 50;

            dgvStages.Columns["id"].Visible = false;
            dgvStages.Columns["id_niveau"].Visible = false;
            dgvStages.Columns["id_categorie_age"].Visible = false;
        }
    }
}
