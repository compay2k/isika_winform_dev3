﻿namespace Fr.Isika.Dev3.Goelands.Presentation
{
    partial class DetailSessions
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.cbxSessions = new System.Windows.Forms.ComboBox();
            this.flpInscrits = new System.Windows.Forms.FlowLayoutPanel();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 52);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "PLOP";
            // 
            // cbxSessions
            // 
            this.cbxSessions.FormattingEnabled = true;
            this.cbxSessions.Location = new System.Drawing.Point(99, 49);
            this.cbxSessions.Name = "cbxSessions";
            this.cbxSessions.Size = new System.Drawing.Size(615, 21);
            this.cbxSessions.TabIndex = 1;
            this.cbxSessions.SelectedIndexChanged += new System.EventHandler(this.CbxSessions_SelectedIndexChanged);
            // 
            // flpInscrits
            // 
            this.flpInscrits.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.flpInscrits.BackColor = System.Drawing.SystemColors.ControlDark;
            this.flpInscrits.Location = new System.Drawing.Point(12, 114);
            this.flpInscrits.Name = "flpInscrits";
            this.flpInscrits.Size = new System.Drawing.Size(795, 412);
            this.flpInscrits.TabIndex = 2;
            // 
            // DetailSessions
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(819, 538);
            this.Controls.Add(this.flpInscrits);
            this.Controls.Add(this.cbxSessions);
            this.Controls.Add(this.label1);
            this.Name = "DetailSessions";
            this.Text = "DetailSessions";
            this.Load += new System.EventHandler(this.DetailSessions_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbxSessions;
        private System.Windows.Forms.FlowLayoutPanel flpInscrits;
    }
}