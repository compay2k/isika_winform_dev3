﻿using Fr.Isika.Dev3.Goelands.DataAccess;
using Fr.Isika.Dev3.Goelands.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fr.Isika.Dev3.Goelands.Presentation
{
    public partial class EditionNiveaux : Form
    {
        private NiveauDAO niveauDao = new NiveauDAO();

        public EditionNiveaux()
        {
            InitializeComponent();
            lblMessage.Text = "";

            // remplir la liste des niveaux :
            // DataBinding
            cbxNiveaux.DisplayMember = "NomComplet";
            MajListeNiveaux();

        }

        private void MajListeNiveaux()
        {
            // je recharge ma liste : 
            cbxNiveaux.DataSource = niveauDao.GetAll();
            cbxNiveaux.SelectedIndex = -1;
        }

        private void BtnEnregistrer_Click(object sender, EventArgs e)
        {
            if (txtLibelle.Text != "")
            {
                Niveau n;
                if (cbxNiveaux.SelectedItem == null)
                {
                    n = new Niveau();
                }
                else
                {
                    n = (Niveau)cbxNiveaux.SelectedItem;
                }
                               
                n.Libelle = txtLibelle.Text;
                n.Description = txtDescription.Text;

                try
                {
                    if (n.Id == 0)
                    {
                        niveauDao.Insert(n);
                    }
                    else
                    {
                        niveauDao.Update(n);
                    }
                    txtLibelle.Clear();
                    txtDescription.Clear();
                    lblMessage.ForeColor = Color.Green;
                    lblMessage.Text = "Niveau enregistré !";
                    // je recharge ma liste : 
                    MajListeNiveaux();
                }
                catch(Exception exc)
                {
                    lblMessage.ForeColor = Color.Red;
                    lblMessage.Text = "ERREUR : " + exc.Message;
                }
            }
            else
            {
                lblMessage.ForeColor = Color.Red;
                lblMessage.Text = "Vous devez remplir le libellé";
            }
        }

        private void CbxNiveaux_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbxNiveaux.SelectedIndex > -1)
            {
                // je récupère le niveau sélectionné dans la combo : 
                Niveau n = (Niveau)cbxNiveaux.SelectedItem;
                // je mets à jour les champs du formulaire :
                txtLibelle.Text = n.Libelle;
                txtDescription.Text = n.Description;
            }
            else
            {
                txtLibelle.Clear();
                txtDescription.Clear();
            }
        }

        private void BtnSupprimer_Click(object sender, EventArgs e)
        {
            if (cbxNiveaux.SelectedItem != null)
            {
                Niveau n = (Niveau)cbxNiveaux.SelectedItem;
                niveauDao.Delete(n.Id);
                MajListeNiveaux();
                lblMessage.ForeColor = Color.Green;
                lblMessage.Text = "Niveau supprimé !";
            }
        }
    }
}
