﻿using Fr.Isika.Dev3.Goelands.DataAccess;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fr.Isika.Dev3.Goelands.Presentation
{
    public partial class GestionStage : Form
    {
        private StageDAO dao = new StageDAO();
        private DataTable dt;
        private int numero;
        public GestionStage()
        {
            InitializeComponent();
        }

        private void BtnRechercher_Click(object sender, EventArgs e)
        {
            dt = dao.rechercherStage(txtLibelleRecherche.Text);
            dataGridView1.DataSource = dt;
        }

        private void DataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                numero = e.RowIndex;
                txtLibelleModif.Text =
                    dataGridView1.Rows[e.RowIndex].Cells["libelle"].Value.ToString();
                txtNbMaxModif.Text =
                    dataGridView1.Rows[e.RowIndex].Cells["nb_max_inscrits"].Value.ToString();
                txtNbMinModif.Text =
                    dataGridView1.Rows[e.RowIndex].Cells["nb_min_inscrits"].Value.ToString();
                txtDureeModif.Text =
                    dataGridView1.Rows[e.RowIndex].Cells["duree"].Value.ToString();
            }
        }

        private void BtnModifier_Click(object sender, EventArgs e)
        {
            //ou dataGridView1.SelectedRows[0].Index
            dt.Rows[numero]["libelle"] = txtLibelleModif.Text;
            dt.Rows[numero]["nb_max_inscrits"] = txtNbMaxModif.Text;
            dt.Rows[numero]["nb_min_inscrits"] = txtNbMinModif.Text;
            dt.Rows[numero]["duree"] = txtDureeModif.Text;
            dao.modifier(dt);
            BtnRechercher_Click(sender, e);
        }
    }
}
