﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Fr.Isika.Dev3.Goelands.Entities;

namespace Fr.Isika.Dev3.Goelands.Presentation
{
    public partial class UCAdherent : UserControl
    {
        private Adherent adherent;

        public Adherent Adherent
        {
            get { return adherent; }
            set
            {
                this.adherent = value;
                txtNom.Text = adherent.Nom;
                txtPrenom.Text = adherent.Prenom;
                dtpDateNaissance.Value = adherent.DateNaissance;
            }
        }


        public UCAdherent()
        {
            InitializeComponent();
        }
    }
}
