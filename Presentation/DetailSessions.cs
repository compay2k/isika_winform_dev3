﻿using Fr.Isika.Dev3.Goelands.DataAccess;
using Fr.Isika.Dev3.Goelands.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fr.Isika.Dev3.Goelands.Presentation
{
    public partial class DetailSessions : Form
    {
        public DetailSessions()
        {
            InitializeComponent();
        }

        private void DetailSessions_Load(object sender, EventArgs e)
        {
            SessionDAO dao = new SessionDAO();

            cbxSessions.DisplayMember = "DateDebut";
            cbxSessions.DataSource = dao.GetAll();
        }

        private void CbxSessions_SelectedIndexChanged(object sender, EventArgs e)
        {
            // je recupère l'id de la session sélectionnée cisuiyqi uqs o :
            Session s = (Session)cbxSessions.SelectedItem;

            // je récupère les adherents inscrits à cette session :
            AdherentDAO dao = new AdherentDAO();
            List<Adherent> inscrits = dao.GetByIdSession(s.Id);

            flpInscrits.Controls.Clear();

            bool pair = (flpInscrits.Controls.Count % 2 == 0);

            foreach(Adherent adh in inscrits)
            {
                UCAdherent uc = new UCAdherent();
                uc.Adherent = adh;
                flpInscrits.Controls.Add(uc);
                if (pair)
                {
                    uc.BackColor = Color.LightPink;
                }
                else
                {
                    uc.BackColor = Color.DarkKhaki;
                }

                pair = !pair;
            }
        }
    }
}
