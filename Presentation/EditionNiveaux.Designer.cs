﻿namespace Fr.Isika.Dev3.Goelands.Presentation
{
    partial class EditionNiveaux
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblLibelle = new System.Windows.Forms.Label();
            this.lblDescription = new System.Windows.Forms.Label();
            this.txtLibelle = new System.Windows.Forms.TextBox();
            this.txtDescription = new System.Windows.Forms.TextBox();
            this.btnEnregistrer = new System.Windows.Forms.Button();
            this.lblMessage = new System.Windows.Forms.Label();
            this.cbxNiveaux = new System.Windows.Forms.ComboBox();
            this.btnSupprimer = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblLibelle
            // 
            this.lblLibelle.AutoSize = true;
            this.lblLibelle.Location = new System.Drawing.Point(70, 97);
            this.lblLibelle.Name = "lblLibelle";
            this.lblLibelle.Size = new System.Drawing.Size(37, 13);
            this.lblLibelle.TabIndex = 0;
            this.lblLibelle.Text = "Libellé";
            // 
            // lblDescription
            // 
            this.lblDescription.AutoSize = true;
            this.lblDescription.Location = new System.Drawing.Point(47, 134);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(60, 13);
            this.lblDescription.TabIndex = 1;
            this.lblDescription.Text = "Description";
            // 
            // txtLibelle
            // 
            this.txtLibelle.Location = new System.Drawing.Point(114, 94);
            this.txtLibelle.Name = "txtLibelle";
            this.txtLibelle.Size = new System.Drawing.Size(196, 20);
            this.txtLibelle.TabIndex = 2;
            // 
            // txtDescription
            // 
            this.txtDescription.Location = new System.Drawing.Point(114, 130);
            this.txtDescription.Name = "txtDescription";
            this.txtDescription.Size = new System.Drawing.Size(196, 20);
            this.txtDescription.TabIndex = 3;
            // 
            // btnEnregistrer
            // 
            this.btnEnregistrer.Location = new System.Drawing.Point(50, 167);
            this.btnEnregistrer.Name = "btnEnregistrer";
            this.btnEnregistrer.Size = new System.Drawing.Size(204, 70);
            this.btnEnregistrer.TabIndex = 4;
            this.btnEnregistrer.Text = "ENREGISTRER";
            this.btnEnregistrer.UseVisualStyleBackColor = true;
            this.btnEnregistrer.Click += new System.EventHandler(this.BtnEnregistrer_Click);
            // 
            // lblMessage
            // 
            this.lblMessage.AutoSize = true;
            this.lblMessage.Location = new System.Drawing.Point(111, 275);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(102, 13);
            this.lblMessage.TabIndex = 5;
            this.lblMessage.Text = "Message de résultat";
            // 
            // cbxNiveaux
            // 
            this.cbxNiveaux.FormattingEnabled = true;
            this.cbxNiveaux.Location = new System.Drawing.Point(114, 29);
            this.cbxNiveaux.Name = "cbxNiveaux";
            this.cbxNiveaux.Size = new System.Drawing.Size(196, 21);
            this.cbxNiveaux.TabIndex = 6;
            this.cbxNiveaux.SelectedIndexChanged += new System.EventHandler(this.CbxNiveaux_SelectedIndexChanged);
            // 
            // btnSupprimer
            // 
            this.btnSupprimer.BackgroundImage = global::Fr.Isika.Dev3.Goelands.Presentation.Properties.Resources.poubelle;
            this.btnSupprimer.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnSupprimer.Location = new System.Drawing.Point(270, 167);
            this.btnSupprimer.Name = "btnSupprimer";
            this.btnSupprimer.Size = new System.Drawing.Size(70, 70);
            this.btnSupprimer.TabIndex = 7;
            this.btnSupprimer.UseVisualStyleBackColor = true;
            this.btnSupprimer.Click += new System.EventHandler(this.BtnSupprimer_Click);
            // 
            // EditionNiveaux
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1163, 536);
            this.Controls.Add(this.btnSupprimer);
            this.Controls.Add(this.cbxNiveaux);
            this.Controls.Add(this.lblMessage);
            this.Controls.Add(this.btnEnregistrer);
            this.Controls.Add(this.txtDescription);
            this.Controls.Add(this.txtLibelle);
            this.Controls.Add(this.lblDescription);
            this.Controls.Add(this.lblLibelle);
            this.Name = "EditionNiveaux";
            this.Text = "EditionNiveaux";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblLibelle;
        private System.Windows.Forms.Label lblDescription;
        private System.Windows.Forms.TextBox txtLibelle;
        private System.Windows.Forms.TextBox txtDescription;
        private System.Windows.Forms.Button btnEnregistrer;
        private System.Windows.Forms.Label lblMessage;
        private System.Windows.Forms.ComboBox cbxNiveaux;
        private System.Windows.Forms.Button btnSupprimer;
    }
}