﻿using Fr.Isika.Dev3.Goelands.Entities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.Isika.Dev3.Goelands.DataAccess
{
    public class SessionDAO : DAO
    {
        public List<Session> GetAll()
        {
            List<Session> result = new List<Session>();
            MySqlCommand cmd = GetCommand(@"SELECT * FROM session");

            try
            {
                cmd.Connection.Open();
                MySqlDataReader dr = cmd.ExecuteReader();
                while(dr.Read())
                {
                    Session s = new Session();
                    s.Id = (int)dr["id"];
                    s.DateDebut = (DateTime)dr["date_debut"];
                    s.IdStage = (int)dr["id_stage"];

                    if (!dr.IsDBNull(dr.GetOrdinal("id_moniteur")))
                    {
                        s.IdMoniteur = (int)dr["id_moniteur"];
                    }

                    result.Add(s);
                }
                
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }

            return result;
        }
    }
}
