﻿using Fr.Isika.Dev3.Goelands.Entities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.Isika.Dev3.Goelands.DataAccess
{
    public class AdherentDAO : DAO
    {
        public List<Adherent> GetByIdSession(int idSession)
        {
            List<Adherent> result = new List<Adherent>();

            MySqlCommand cmd = GetCommand(@"SELECT adh.id, adh.nom, adh.prenom, adh.date_naissance
                                                FROM adherent adh
                                                INNER JOIN inscription ins ON adh.id = ins.id_adherent
                                                WHERE ins.id_session = @id_session
                                                ");

            AjouterParametre(cmd, "id_session", idSession);

            try
            {
                cmd.Connection.Open();

                MySqlDataReader dr = cmd.ExecuteReader();

                while(dr.Read())
                {
                    Adherent a = new Adherent();

                    a.Id = (int)dr["id"];
                    a.Nom = (string)dr["nom"];
                    a.Prenom = (string)dr["prenom"];
                    a.DateNaissance = (DateTime)dr["date_naissance"];

                    result.Add(a);
                }
            }
            catch(Exception exc)
            {
                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }

            return result;
        }

    }
}
