﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.Isika.Dev3.Goelands.DataAccess
{
    public class StageDAO : DAO
    {
        #region Travail de Benoit.C
        public DataTable GetDetails()
        {
            MySqlCommand cmd = this.GetCommand(@"SELECT stg.id, stg.libelle 'libellé', stg.duree 'durée', 
                                                    stg.nb_min_inscrits 'min.', stg.nb_max_inscrits 'max.', 
                                                    stg.id_niveau, stg.id_categorie_age, niv.libelle 'niveau', 
                                                    cat.libelle 'cat. age'
                                                 FROM stage stg
                                                      INNER JOIN niveau niv on stg.id_niveau = niv.id
                                                      LEFT JOIN categorie_age cat on stg.id_categorie_age = cat.id 
                                                 ORDER BY stg.libelle");

            // je crée un dataAdapter :
            MySqlDataAdapter da = new MySqlDataAdapter();
            // je lui fournis la commande à exécuter :
            da.SelectCommand = cmd;
            // je crée une dataTable :
            DataTable dt = new DataTable();
            // je demande au dataAdapter de remplir la table :
            da.Fill(dt);

            return dt;
        }
        #endregion

        public MySqlDataAdapter dataAdapter { get; set; }
        /// <summary>
        /// Default Constructor.
        /// </summary>
        public StageDAO()
        {
            dataAdapter = new MySqlDataAdapter();
            // SELECT COMMAND
            dataAdapter.SelectCommand = GetCommand(
                @"SELECT id, libelle, nb_min_inscrits, 
                         nb_max_inscrits, duree, id_niveau, id_categorie_age 
                  FROM stage WHERE libelle like @paramLibelle");
            dataAdapter.SelectCommand.Parameters.Add(
                new MySqlParameter("paramLibelle", MySqlDbType.VarChar));
            // UPDATE COMMAND
            dataAdapter.UpdateCommand = GetCommand(
                @"UPDATE stage set libelle = @libelle, nb_min_inscrits = @nb_min_inscrits, 
                                   nb_max_inscrits = @nb_max_inscrits, duree = @duree 
                  WHERE id = @id");
            dataAdapter.UpdateCommand.Parameters.Add(
                new MySqlParameter("libelle", MySqlDbType.VarChar));
            dataAdapter.UpdateCommand.Parameters["libelle"].SourceColumn = "libelle";
            dataAdapter.UpdateCommand.Parameters.Add(
                new MySqlParameter("nb_min_inscrits", MySqlDbType.Int32));
            dataAdapter.UpdateCommand.Parameters["nb_min_inscrits"].SourceColumn = "nb_min_inscrits";
            dataAdapter.UpdateCommand.Parameters.Add(
                new MySqlParameter("nb_max_inscrits", MySqlDbType.Int32));
            dataAdapter.UpdateCommand.Parameters["nb_max_inscrits"].SourceColumn = "nb_max_inscrits";
            dataAdapter.UpdateCommand.Parameters.Add(
                new MySqlParameter("duree", MySqlDbType.Int32));
            dataAdapter.UpdateCommand.Parameters["duree"].SourceColumn = "duree";
            dataAdapter.UpdateCommand.Parameters.Add(
                new MySqlParameter("id", MySqlDbType.Int32));
            dataAdapter.UpdateCommand.Parameters["id"].SourceColumn = "id";
            // DELETE COMMAND 
            dataAdapter.DeleteCommand = GetCommand("");
        }
        public DataTable rechercherStage(String libelle)
        {
            DataTable dt = new DataTable();
            dataAdapter.SelectCommand.Parameters["paramLibelle"].Value = 
                "%" + libelle + "%";
            dataAdapter.Fill(dt);
            return dt;
        }

        public void modifier(DataTable dt)
        {
            dataAdapter.Update(dt);
        }
    }
}
