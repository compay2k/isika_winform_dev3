﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.Isika.Dev3.Goelands.DataAccess
{
    public abstract class DAO
    {
        protected string connectionString = "Server=127.0.0.1;Database=voile;Uid=root;CharSet=utf8;";

        protected MySqlConnection GetConnection()
        {
            MySqlConnection cnx = new MySqlConnection();
            cnx.ConnectionString = this.connectionString;
            return cnx;
        }

        protected MySqlCommand GetCommand(string sql)
        {
            MySqlCommand cmd = new MySqlCommand();
            cmd.Connection = this.GetConnection();
            cmd.CommandText = sql;
            return cmd;
        }

        protected void AjouterParametre(MySqlCommand cmd, string nom, object valeur)
        {
            cmd.Parameters.Add(new MySqlParameter("@" + nom, valeur));
        }

    }
}
