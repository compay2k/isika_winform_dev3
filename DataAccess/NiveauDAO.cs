﻿using Fr.Isika.Dev3.Goelands.Entities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.Isika.Dev3.Goelands.DataAccess
{
    public class NiveauDAO : DAO
    {
        
        public List<Niveau> GetAll()
        {
            List<Niveau> result = new List<Niveau>();
            MySqlCommand cmd = this.GetCommand(@"SELECT * FROM niveau
                                                ORDER BY libelle");

            try
            {
                cmd.Connection.Open();

                MySqlDataReader dr = cmd.ExecuteReader();

                while(dr.Read())
                {
                    Niveau n = new Niveau();
                    n.Id = (int)dr["id"];
                    n.Libelle = (string)dr["libelle"];
                    n.Description = (string)dr["description"];

                    result.Add(n);
                }
            }
            catch(Exception exc)
            {
                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }

            return result;
        }

        public Niveau GetById(int id)
        {
            Niveau result = null;
            MySqlCommand cmd = this.GetCommand(@"SELECT * FROM niveau WHERE id = @id");

            // ajout du paramètre :
            AjouterParametre(cmd, "id", id);

            try
            {
                cmd.Connection.Open();

                MySqlDataReader dr = cmd.ExecuteReader();

                if (dr.Read())
                {
                    result = new Niveau();
                    result.Id = (int)dr["id"];
                    result.Libelle = (string)dr["libelle"];
                    result.Description = (string)dr["description"];
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }

            return result;
        }

        public void Insert(Niveau niveau)
        {
            MySqlCommand cmd = this.GetCommand(@"INSERT INTO niveau 
                                                 (libelle, description)
                                                 VALUES
                                                 (@libelle, @description)
                                                    ;
                                                 SELECT LAST_INSERT_ID()");

            AjouterParametre(cmd, "libelle", niveau.Libelle);
            AjouterParametre(cmd, "description", niveau.Description);

            try
            {
                cmd.Connection.Open();
                niveau.Id = Convert.ToInt32(cmd.ExecuteScalar());
            }
            catch(Exception exc)
            {
                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }
        }

        public void Update(Niveau niveau)
        {
            MySqlCommand cmd = this.GetCommand(@"UPDATE niveau 
                                                 SET
                                                 libelle = @libelle,
                                                 description = @description
                                                 WHERE id = @id");

            AjouterParametre(cmd, "libelle", niveau.Libelle);
            AjouterParametre(cmd, "description", niveau.Description);
            AjouterParametre(cmd, "id", niveau.Id);

            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
            }
            catch(Exception exc)
            {
                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }
        }

        public void Delete(int id)
        {
            // je créee ma commande sql :
            MySqlCommand cmd = this.GetCommand(@"DELETE FROM niveau WHERE id = @id");

            // j'ajoute le(s) parametre(s) :
            AjouterParametre(cmd, "id", id);

            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
            }
            catch(Exception exc)
            {
                throw exc;
            }
            finally
            {
                cmd.Connection.Close();
            }
        }
    }
}
