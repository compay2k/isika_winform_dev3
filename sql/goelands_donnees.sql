INSERT INTO niveau (libelle, description) VALUES ('1 etoile','pour les debutants');
INSERT INTO niveau (libelle, description) VALUES ('2 etoiles','pour les confirmés');
INSERT INTO niveau (libelle, description) VALUES ('3 etoiles','pour les avancés');
INSERT INTO niveau (libelle, description) VALUES ('4 etoiles','pour les pros');

INSERT INTO categorie_age (libelle, age_min, age_max) VALUES ('poussin',0,3);
INSERT INTO categorie_age (libelle, age_min, age_max) VALUES ('junior',4,7);
INSERT INTO categorie_age (libelle, age_min, age_max) VALUES ('benjamin',8,12);
INSERT INTO categorie_age (libelle, age_min, age_max) VALUES ('teens',13,17);
INSERT INTO categorie_age (libelle, age_min, age_max) VALUES ('senior',18,null);

INSERT INTO thematique (libelle) VALUES 
('hotdogs'), 
('burgers'),
('decoupage'),
('kebab'),
('friture'),
('nappage'),
('cuisson à l\'huile'),
('cocktail'),
('sirop'),
('kouign amann');

INSERT INTO stage(libelle, duree, nb_min_inscrits, nb_max_inscrits, id_niveau, id_categorie_age) 
VALUES 
('mes premiers nuggets', 3, 4, 8, 1, 1),
('food trucks et santé publique', 2, 4, 12, 1, 5),
('beurre et traditions', 4, 5, 9, 3, null),
('le cholesterol pour les nuls', 3, 4, 10, 3, 5),
('ti punch par la pratique', 8, 12, 25, 3, 3);

INSERT INTO thematique_stage(id_stage, id_thematique) 
VALUES 
(1, 5),
(1, 7),
(2, 1),
(2, 2),
(2, 4),
(2, 5),
(4, 5),
(4, 6),
(4, 7),
(4, 8),
(4, 9),
(4, 10);

INSERT INTO `adherent`(`nom`, `prenom`, `email`, `date_naissance`, `date_adhesion`, `date_cotisation`) 
VALUES 
('McDonald', 'Ronald', 'rmd@rmd.com', '1952-02-23', '2018-10-15', NULL),
('Vincent', 'Franckie', 'fv@vf.com', '1952-02-23', '2018-10-15', NULL),
('Dylan', 'Bob', 'rmd@rmd.com', '1952-02-23', '2018-10-15', NULL),
('Tsonga', 'Jo Wilfried', 'rmd@rmd.com', '1952-02-23', '2018-10-15', NULL),
('Maïte', 'Maïte', 'rmd@rmd.com', '1952-02-23', '2018-10-15', NULL),
('Etchebest', 'Philippe', 'rmd@rmd.com', '1952-02-23', '2018-10-15', NULL); 

INSERT INTO moniteur(id_adherent)
VALUES
(1), (6);

INSERT INTO session(date_debut, tarif, id_stage, id_moniteur) 
VALUES 
('2019-07-12', 350, 3, 1),
('2019-08-15', 500, 3, 1),
('2019-11-05', 250, 3, NULL),
('2019-04-15', 200, 2, 2),
('2019-05-10', 200, 2, 2),
('2019-06-08', 280, 2, 1),
('2019-07-03', 320, 2, NULL),
('2019-07-09', 630, 5, NULL);

INSERT INTO `inscription`(`montant_verse`, `date_inscription`, `date_paiement`, `id_session`, `id_adherent`) 
VALUES 
(350, '2019-05-12', '2019-05-12', 1, 3),
(450, '2019-03-05', '2019-03-05', 1, 4),
(220, '2019-05-12', '2019-05-12', 1, 2),
(550, '2019-05-12', '2019-05-12', 3, 3),
(350, '2019-05-12', '2019-05-12', 3, 2),
(550, '2019-05-12', '2019-05-12', 8, 3),
(350, '2019-05-12', '2019-05-12', 8, 2);
