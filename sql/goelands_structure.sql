drop database if exists voile;

create database voile;

use voile;

create table niveau(
  id int not null auto_increment,
  libelle varchar(50) not null,
  description varchar(1024) not null,
  primary key(id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

create table categorie_age(
  id int not null auto_increment,
  libelle varchar(50) not null,
  age_min int,
  age_max int,
  primary key(id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

create table stage(
  id int not null auto_increment,
  libelle varchar(255) not null,
  duree int not null,
  nb_min_inscrits int not null default 0,
  nb_max_inscrits int not null,
  id_niveau int not null,
  id_categorie_age int,
  primary key(id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

alter table stage add constraint fk_stage_niveau  
FOREIGN KEY (id_niveau) REFERENCES niveau(id);

alter table stage add constraint fk_stage_categorie_age  
FOREIGN KEY (id_categorie_age) REFERENCES categorie_age(id);

create table `session`(
  id int not null auto_increment,
  date_debut date not null,
  tarif float,
  id_stage int not null,
  id_moniteur int,
  primary key(id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

alter table `session` add constraint fk_session_stage  
FOREIGN KEY (id_stage) REFERENCES stage(id);

create table adherent(
  id int not null auto_increment,
  nom varchar(50) not null,
  prenom varchar(50) not null,
  email varchar(50) not null,
  date_naissance date not null,
  date_adhesion date default '2001-02-02',
  date_cotisation date,
  primary key(id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

create table inscription(
  id int not null auto_increment,
  montant_verse float default 0,
  date_inscription date not null,
  date_paiement date,
  id_session int not null,
  id_adherent int not null,
  primary key(id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

alter table inscription add constraint fk_inscription_session  
FOREIGN KEY (id_session) REFERENCES `session`(id);

alter table inscription add constraint fk_inscription_adherent  
FOREIGN KEY (id_adherent) REFERENCES adherent(id);

create table moniteur(
  id int not null auto_increment,
  id_adherent int not null,
  primary key(id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

alter table moniteur add constraint fk_moniteur_adherent  
FOREIGN KEY (id_adherent) REFERENCES adherent(id);

alter table `session` add constraint fk_session_moniteur  
FOREIGN KEY (id_moniteur) REFERENCES moniteur(id);

create table thematique(
  id int not null auto_increment,
  libelle varchar(50) not null,
  primary key(id)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

create table thematique_stage(
    id_thematique int not null,
    id_stage int not null,
    primary key(id_thematique, id_stage)
)ENGINE=InnoDB DEFAULT CHARSET=utf8;

alter table thematique_stage add constraint fk_thematique_stage_thematique  
FOREIGN KEY (id_thematique) REFERENCES thematique(id);

alter table thematique_stage add constraint fk_thematique_stage_stage  
FOREIGN KEY (id_stage) REFERENCES stage(id);

commit;